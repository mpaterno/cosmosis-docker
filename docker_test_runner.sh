die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "python -V 2>& 1 | fgrep 2.7.14" || die "wrong version of python" 

docker run ${STD_ARGS} ${IMAGE} bash -cl "gcc --version | fgrep 7.2.0" || die "wrong version of gcc" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "g++ --version | fgrep 7.2.0" || die "wrong version of g++" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "gfortran --version | fgrep 7.2.0" || die "wrong version of gfortran" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "python -c 'import numpy as np; np.lib.test()'" || die "numpy not installed" 
docker run ${STD_ARGS} ${IMAGE} bash -cl "cosmosis --help" || die "cosmosis not installed"

echo "All tests OK"
