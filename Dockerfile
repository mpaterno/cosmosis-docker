# This Dockerfile is a bit like a Makefile, except
# that it contains the recipe to build a Docker Image
# (like a virtual machine)

# The parent or base image contains many but not all of the cosmosis
# dependencies environment variables.  See the Dockerfile in base-image
# for how that was created originally (it will be downloaded from 
# hub.docker.io).

FROM paterno/cosmosis-base:1.8

# OpenBLAS linear algebra installation (provides lapack and blas)
# We could put this in the parent image (joezuntz/cosmosis-test-3) but then
# it would not be tuned as well to your machine.

#RUN cd /opt && git clone https://github.com/xianyi/OpenBLAS && cd OpenBLAS && make && make PREFIX=/usr/local install && rm -rf /opt/OpenBLAS
RUN cd /opt \
   && wget "https://github.com/xianyi/OpenBLAS/archive/v0.2.20.tar.gz" \
   && tar xf v0.2.20.tar.gz \
   && cd OpenBLAS-0.2.20 \
   && make -j $(nproc --all) \
   && make PREFIX=/usr/local install \
   && rm -rf /opt/OpenBLAS-0.2.20

# Many of the python dependencies themselves ultimately depend on LAPACK
# so we need to install them manually here.
# We run these installations in a series of passes. The first pass
# does installations that depend on nothing. Subsequent passes do installations
# that depend on items in previous passes. This allows updates to versions (or
# additions to handle new dependencies) to retain as many Docker image layers
# as possible, while assuring that high-level installations always are updated
# when a lower-level installation requires an update.
RUN pip install asn1crypto==0.24.0 \
                attrs==18.1.0 \
                backports.functools-lru-cache==1.5 \
                cython==0.28.3 \
                enum34==1.1.6 \
                funcsigs==1.0.2 \
                nose==1.3.7 \
                numpy==1.14. \
                pluggy==0.6.0 \
                py==1.5.3 \
                pyparsing==2.2.0 \
                pytz==2018.4 \
                pyyaml==3.12 \
                scikit-learn==0.19.1 \
                setuptools==39.0.1 \
                six==1.11.0 \
                subprocess32==3.5.1

RUN pip install astropy==2.0.7 \
                cycler==0.10.0 \
                emcee==2.2.1 \
                h5py==2.8.0 \
                kiwisolver==1.0.1 \
                llvmlite==0.23.2 \
                python-dateutil==2.7.3 \
                scipy==1.1.0 \
                singledispatch==3.4.0.3 \
                sklearn==0.0

RUN pip install cosmolopy==0.1.104 \
                kombine==0.8.3 \
                matplotlib==2.2.2 \
                numba==0.38.1

RUN pip install healpy==1.12.0

RUN pip freeze --all > /opt/base-pip-modules.txt

# USER-INSTALLED PACKAGES SHOULD GO AFTER HERE.

# If you need more libraries or other 
# tools you can put the commands to install them in here, by 
# prefixing the install commands with "RUN".
# Here are some examples:

# You can install with pip:
RUN pip install ipython==5.5.0


# Or with the apt-get command from ubuntu. You can run the VM and search
# inside for what is available like this:
# ./start-cosmosis-vm
# apt-cache search name_to_search_for
#
# RUN apt-get install -y vim

# For more involved multi-command installations have a look at the OpenBLAS example
# above that uses && to separate the commands.

# This allows continuous bash history  (so you can press up to get previous commands)
# even when re-launching the machine
ENV HISTFILE /cosmosis/.bash_history

# Install the FITSIO library - this is a pain to do with pip
RUN cd /opt && git clone https://github.com/esheldon/fitsio && cd fitsio && python setup.py build_ext   --use-system-fitsio install && rm -rf /opt/fitsio

RUN pip install future configparser

#Uncomment this if you have issues with unicode docstrings
ENV LANG en_US.UTF-8
ENV PYTHONIOENCODING UTF-8

# If you are on Linux and have problems with permissions then
# you can run on your normal machine:
# id -u  # write down the result and use it as XXX below
# id -g  # write down the result and use it as YYY below
# Then modify and uncomment these lines:
#RUN useradd -r -u XXXX -g YYYY user
#USER user


# Here you can add any of your own dependencies/libraries/etc.
RUN cd /tmp \
  && wget http://www.feynarts.de/cuba/Cuba-4.2.tar.gz \
  && tar xf Cuba-4.2.tar.gz \
  && rm Cuba-4.2.tar.gz \
  && cd Cuba-4.2 \
  && CFLAGS="-O3 -ffast-math -fomit-frame-pointer -march=native -fPIC" ./configure \
  && make lib \
  && make install \
  && cd /usr/local/lib \
  && mkdir /tmp/foo \
  && mv libcuba.a /tmp/foo \
  && cd /tmp/foo \
  && ar xv libcuba.a \
  && gcc -shared *.o -o libcuba.so \
  && rm -f *.o \
  && mv libcuba.so /usr/local/lib \
  && rm -rf /tmp/Cuba-4.2 \
  && rm -rf /tmp/foo

RUN apt-get -y update \
    && apt-get -y install cmake

RUN apt-get -y update \
    && apt-get -y install libffi-dev

RUN apt-get -y update \
    && apt-get -y install vim

RUN mkdir -p /tmp/build \
    && cd /tmp/build \
    && git clone https://github.com/google/benchmark.git \
    && cd benchmark \
    && mkdir build \
    && cd build \
    && cmake .. -DCMAKE_BUILD_TYPE=RELEASE -DBENCHMARK_DOWNLOAD_DEPENDENCIES=ON \
    && make \
    && make install \
    && cd / \
    && rm -rf /tmp/build

RUN apt-get -y update \
    && apt-get -y install gdb

# Installing `gperftools`
RUN apt-get -y install google-perftools libgoogle-perftools-dev graphviz

# There is a bug in OpenBlas with threading
# that can affect the fortran parts of the Planck likelihood code
# This seems to help
ENV OPENBLAS_NUM_THREADS 1

# Update to current version of previously-installed packages.
RUN apt-get -y update && \
    apt-get -y upgrade

# Install Eigen3 development library, as needed for cubacpp.
RUN apt-get -y update && \
    apt-get -y install libeigen3-dev

# We check out a specific SHA, because cluster_toolkit does not have
# numbered versions, and we want to make sure everyone is using the
# same version of cluster_tookit.
# This is moved to after the installation of things for which we are
# less version-sensitive, in case we need to move to a different
# version.
RUN cd /opt \
    && git clone https://github.com/tmcclintock/cluster_toolkit.git \
    && cd cluster_toolkit \
    && git checkout 9c139d61e35fcbc32d542d8f4956b12c7fbe6701 \
    && python setup.py install \
    && rm -rf /opt/cluster_toolkit

# This is the one case where ADD is better than COPY;
# we want to automatically unpack the tarball.
# ADD CLion-2019.1.1.tar /opt/ 
# RUN apt-get update \
#   && apt-get install -y xorg

# We need a newer version of CMake than is provided by our package manager.
RUN cd /usr && \
    wget https://github.com/Kitware/CMake/releases/download/v3.15.3/cmake-3.15.3-Linux-x86_64.tar.gz && \
    tar --strip-components=1 -x -f cmake-3.15.3-Linux-x86_64.tar.gz && \
    rm cmake-3.15.3-Linux-x86_64.tar.gz
